import SpriteKit

protocol ProgressBarDelegate
{
    func progressBarCountDownDidArrived(progressBar:ProgressBar)
}

class ProgressBar:SKNode {
    
    // The image Sprite and the mask Sprite
    let progressBackgroundLayer: SKSpriteNode
    
    let progressBarContent: ProgressBarContent
    
    var timer: NSTimer?
    
    var currentProgress:CGFloat = 1.0
    
    var deleage: ProgressBarDelegate?
    
    // Constructor takes an SKSpriteNode as a parameter
    init(progressBar: SKSpriteNode) {
        self.progressBackgroundLayer = SKSpriteNode(color: UIColor.whiteColor(), size: progressBar.size)
        self.progressBarContent = ProgressBarContent(progressBar: progressBar)
        super.init()
        progressBackgroundLayer.position = CGPoint(x:CGRectGetMidX(progressBar.frame) , y: CGRectGetMidY(progressBar.frame) + 8.0)
        self.addChild(progressBackgroundLayer)
        progressBackgroundLayer.addChild(progressBarContent)
    }
    
    func startCountDown()
    {
        if timer != nil {
            self.resetCountDown()
        }
        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("decreaseProgress"), userInfo: nil, repeats: true)
    }
    
    func resetCountDown()
    {
        currentProgress = 1.0
        self.updateProgress()
        timer?.invalidate()
        self.timer = nil
    }
    
    // This method updates the mask's xScale
    func updateProgress() -> Void {
        progressBarContent.mask.xScale = currentProgress;
    }
    
    func decreaseProgress()
    {
        if currentProgress <= 0 {
            deleage?.progressBarCountDownDidArrived(self)
            return
        }
        currentProgress -= 0.02
        self.updateProgress()
    }
    
}

class ProgressBarContent: SKCropNode
{
    // The image Sprite and the mask Sprite
    var progressBar : SKSpriteNode
    var mask : SKSpriteNode
    
    // Constructor takes an SKSpriteNode as a parameter
    init(progressBar: SKSpriteNode) {
        self.progressBar = progressBar
        // Create the mask based on the progressBar Sprite
        self.mask = SKSpriteNode(color: SKColor.redColor(), size: CGSizeMake(self.progressBar.size.width, self.progressBar.size.height))
        self.mask.position = CGPoint(x: self.progressBar.position.x - self.progressBar.size.width/2, y: self.progressBar.position.y)
        self.mask.anchorPoint = CGPoint(x: 0.0, y: 0.5)
        
        // Initialise super class
        super.init()
        
        // Add children
        self.addChild(progressBar)
        self.maskNode = self.mask
        
    }
    
    // This method updates the mask's xScale
    func updateProgress( progress: CGFloat ) -> Void {
        self.mask.xScale = progress;
    }
}