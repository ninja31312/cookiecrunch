//
//  Array2D.swift
//  CookieCrunch
//
//  Created by Matthijs on 19-06-14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

class Array2D<T> {
  let columns: Int
  let rows: Int
  //In Beta3 constant arrays are completely immutable while variable arrays are entirely mutable. Change let array: [T?] to var array: [T?] and your code should work.
  var array: Array<T?>   // private

  init(columns: Int, rows: Int) {
    self.columns = columns
    self.rows = rows
    array = Array<T?>(count: rows*columns, repeatedValue: nil)
  }

  subscript(column: Int, row: Int) -> T? {
    get {
      return array[row*columns + column]
    }
    set {
      array[row*columns + column] = newValue
    }
  }
}
