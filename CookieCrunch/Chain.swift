/*!
Chain means a group of consecutive cookies.
We may have different kind of chain. Horizontal, Vertical, T, L type 
*/
import Foundation

class Chain : Printable, Hashable
{
	enum ChainType: Printable
	{
		case Horizontal
		case Vertical

		var description: String
		{
			switch self
			{
			case .Horizontal: return "Horizontal"
			case .Vertical: return "Vertical"
			}
		}
	}
	var cookies = [Cookie]()
	var chainType: ChainType

	init(chainType: ChainType)
	{
		self.chainType = chainType
	}

	func firstCookie()->Cookie
	{
		return cookies[0]
	}

	func lastCookie()->Cookie
	{
		return cookies[self.length() - 1]
	}

	func length()->Int
	{
		return cookies.count
	}

	func addCookie(cookie:Cookie)
	{
		cookies.append(cookie)
	}

	var description:String{
		return "type:\(chainType) cookies:\(cookies)"
	}

	var hashValue: Int
	{
		return reduce(cookies, 0) { $0.hashValue ^ $1.hashValue }
	}
}

func == (lhs: Chain, rhs:Chain)->Bool
{
	return lhs.cookies == rhs.cookies
}
